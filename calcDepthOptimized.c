// CS 61C Fall 2015 Project 4

// include SSE intrinsics
#if defined(_MSC_VER)
#include <intrin.h>
#elif defined(__GNUC__) && (defined(__x86_64__) || defined(__i386__))
#include <x86intrin.h>
#endif

// include OpenMP
#if !defined(_MSC_VER)
#include <pthread.h>
#endif
#include <omp.h>

#include <math.h>
#include <stdbool.h>
#include <stdio.h>

#include "calcDepthOptimized.h"
#include "calcDepthNaive.h"

/* DO NOT CHANGE ANYTHING ABOVE THIS LINE. */

float getSquaredDifference (float *left, float *right, int x, int y, int dx, int dy, int featureWidth, int featureHeight, int imageWidth);

// Implements the displacement function
float displacementOptimized(int dx, int dy)
{
    float squaredDisplacement = dx * dx + dy * dy;
    float displacement = sqrt(squaredDisplacement);
    return displacement;
}

float displacementNoSqrtOptimized(int dx, int dy)
{
    return dx * dx + dy * dy;
}

void calcDepthOptimized(float *depth, float *left, float *right, int imageWidth, int imageHeight, int featureWidth, int featureHeight, int maximumDisplacement)
{
    
    /* The two outer for loops iterate through each pixel */
    omp_set_num_threads(8);
#pragma omp parallel for
    for (int y = 0; y < imageHeight; y++)
    {
        for (int x = 0; x < imageWidth; x++)
        {
            
            //Set the depth to 0 if looking at edge of the image where a feature box cannot fit.
            if ((y < featureHeight) || (y >= imageHeight - featureHeight) || (x < featureWidth) || (x >= imageWidth - featureWidth))
            {
                depth[y * imageWidth + x] = 0;
                continue;
            }
            
            float minimumSquaredDifference = -1;
            int minimumDy = 0;
            int minimumDx = 0;
            
            /* Iterate through all feature boxes that fit inside the maximum displacement box.
             centered around the current pixel. */
            
            int yMinusFeatureHeight = y - featureHeight;
            int yPlusFeatureHeight = y + featureHeight;
            int xPlusFeatureWidth = x + featureWidth;
            int xMinusFeatureWidth = x - featureWidth;
            
            for (int dy = -maximumDisplacement; dy <= maximumDisplacement; dy++)
            {
                int yPlusDyMinusFeatureHeight = yMinusFeatureHeight + dy;
                int yPlusDyPlusFeatureHeight = yPlusFeatureHeight + dy;
                
                for (int dx = -maximumDisplacement; dx <= maximumDisplacement; dx++)
                {
                    //Skip feature boxes that dont fit in the displacement box.
                    if (yPlusDyMinusFeatureHeight < 0 || yPlusDyPlusFeatureHeight >= imageHeight || xMinusFeatureWidth + dx < 0 || xPlusFeatureWidth + dx >= imageWidth)
                    {
                        continue;
                    }
                    
                    float squaredDifference = 0;
                    
                    /* Sum the squared difference within a box of +/- featureHeight and +/- featureWidth. */
                    int centerX = x + dx;
                    int centerY = y + dy;
                    
                    int boxY;
                    for (boxY = -featureHeight; boxY <= featureHeight; boxY ++)
                    {
                        int leftY = y + boxY;
                        int rightY = centerY + boxY;
                        
                        int leftYTimesImageWidth = leftY * imageWidth;
                        int rightYTimesImageWidth = rightY * imageWidth;
                        
                        //first width traversal
                        int boxX;
                        for (boxX = -featureWidth; boxX <= featureWidth - 3; boxX += 4)
                        {
                            
                            int leftX = x + boxX;
                            int rightX = centerX + boxX;
                            
                            __m128 xmmReg0 = _mm_loadu_ps (&left[leftYTimesImageWidth + leftX]);
                            __m128 xmmReg1 = _mm_loadu_ps (&right[rightYTimesImageWidth + rightX]);
                            __m128 xmmReg2 = _mm_sub_ps (xmmReg0, xmmReg1);
                            
                            float diff[4];
                            _mm_store_ps (diff, xmmReg2);
                            squaredDifference += diff[0] * diff[0] + diff[1] * diff[1] + diff[2] * diff[2] + diff[3] * diff[3];
                        }
                        
                        //leftover
                        for (; boxX <= featureWidth; boxX++)
                        {
                            int leftX = x + boxX;
                            int rightX = centerX + boxX;
                            
                            float difference = left[leftYTimesImageWidth + leftX] - right[rightYTimesImageWidth + rightX];
                            squaredDifference += difference * difference;
                        }
                    }
                    
                    /*
                     Check if you need to update minimum square difference.
                     This is when either it has not been set yet, the current
                     squared displacement is equal to the min but the new
                     displacement is less, or the current squared difference
                     is less than the min square difference.
                     */
                    if ((minimumSquaredDifference == -1) || (minimumSquaredDifference > squaredDifference) || ((minimumSquaredDifference == squaredDifference) && (displacementNoSqrtOptimized(dx, dy) < displacementNoSqrtOptimized(minimumDx, minimumDy))))
                    {
                        minimumSquaredDifference = squaredDifference;
                        minimumDx = dx;
                        minimumDy = dy;
                    }
                }
            }
            
            /*
             Set the value in the depth map.
             If max displacement is equal to 0, the depth value is just 0.
             */
            if (minimumSquaredDifference != -1)
            {
                if (maximumDisplacement == 0)
                {
                    depth[y * imageWidth + x] = 0;
                }
                else
                {
                    depth[y * imageWidth + x] = displacementOptimized(minimumDx, minimumDy);
                }
            }
            else
            {
                depth[y * imageWidth + x] = 0;
            }
        }
    }
}
/* SAVED CODE
    int boxY;
    for (boxY = -featureHeight; boxY <= featureHeight - 3; boxY += 4)
    {
        __m128i reg0, reg1, reg2, reg3, reg4;
        
        reg0 = _mm_set_epi32 (boxY, boxY+1, boxY+2, boxY+3);    //4 boxY nums
        reg1 = _mm_set1_epi32 (y);                              //y
        reg2 = _mm_set1_epi32 (centerY);                        //centerY
        reg0 = _mm_add_epi32(reg0, reg1);                       //leftY
        reg1 = _mm_add_epi32(reg0, reg2);                       //rightY
        
        //int leftY = y + boxY;
        //int rightY = centerY + boxY;
        
        reg2 = _mm_set1_epi32 (imageWidth);             //imageWidth
        reg3 = _mm_mul_epi32 (reg0, reg2);              //leftYTimesImageWidth
        reg4 = _mm_mul_epi32 (reg1, reg2);              //rightYTimesImageWidth
        
        
        int leftYTimesImageWidth[4];
        int rightYTimesImageWidth[4];
        _mm_storeu_si128 ((__m128i *) leftYTimesImageWidth, reg3);
        _mm_storeu_si128 ((__m128i *) rightYTimesImageWidth, reg4);
        
        //int leftYTimesImageWidth = leftY * imageWidth;
        //int rightYTimesImageWidth = rightY * imageWidth;
        
        //first width traversal
}
*/
